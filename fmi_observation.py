# encoding=utf8
import xml.etree.ElementTree as etree
import datetime
import requests

class fmi_observation():

    def __init__(self, apikey, geoid):
        """fmi_observation, a fetch and parse weatherobservation from FMI (Finnish Meteorologigal Institute) open data service.

        Args:
            apikey (string): apikey from fmi, see http://en.ilmatieteenlaitos.fi/open-data-manual
            lat (string): Latitude coordinate for the observation
            lon (string): Longitude coordinate for the observation

        Returns:
            None
        """
        self.apikey = apikey
        self.geoid = geoid

        self.url_template = 'http://data.fmi.fi/fmi-apikey/{}/wfs?request=getFeature&storedquery_id=fmi::observations::weather::timevaluepair&geoid={}'
        self.url = self.url_template.format(apikey, geoid)

        self.observation_xml = '' # this holds the xml document as a string
        self.observation = None # the parsed observation
        self.serie_names = [] # names for the observation data series

        self.ts_start = None # observation start time
        self.ts_end = None # observation end time
        
        self.needs_update = True # should we get a fresh observation from fmi

  
    def _fetch_from_fmi(self):
        """Fetch xml observation document from FMI open data service, http://en.ilmatieteenlaitos.fi/open-data-manual
        """
        r = requests.get(self.url)
        r.raise_for_status()
        self.observation_xml = r.text


    def _parser_helper(self, item1, item2):
        #print((item1.text))
        return item1.text, item2.text
    

    def _parse_one_series(self, series):
        self.serie_names.append(series.get('{http://www.opengis.net/gml/3.2}id'))
        return [self._parser_helper(item1, item2) for item1, item2 in
                zip(series.iter(tag='{http://www.opengis.net/waterml/2.0}time'),
                    series.iter(tag='{http://www.opengis.net/waterml/2.0}value'))]


    def get_observations(self):
        if self.needs_update:
            self._fetch_from_fmi()
            
        root = etree.fromstring(self.observation_xml)
        tree = etree.ElementTree(root)
        self.serie_names = []
        data = list(zip(*(self._parse_one_series(series) for series in
                    tree.iter(tag='{http://www.opengis.net/waterml/2.0}MeasurementTimeseries'))))

        for timeperiod in root.iter(tag='{http://www.opengis.net/gml/3.2}TimePeriod'):
            start = list(timeperiod)[0].text
            end = list(timeperiod)[1].text
            #2018-03-19T10:00:00Z
            self.ts_start = datetime.datetime.strptime(start, "%Y-%m-%dT%H:%M:%SZ")
            self.ts_end = datetime.datetime.strptime(end, "%Y-%m-%dT%H:%M:%SZ")
            
        #print times
        return data
